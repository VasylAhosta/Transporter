//
//  Explorer.swift
//  Transporter
//
//  Created by Sauron Black on 5/6/15.
//  Copyright (c) 2015 Totenkopf. All rights reserved.
//

import Foundation
import MobileCoreServices

class Explorer
{
    enum ItemType: CustomStringConvertible
    {
        case Directory
        case Image
        case Audio
        case Movie
        case Other
        var description: String {
            switch self {
                case .Directory: return "Directory"
                case .Image: return "Image"
                case .Audio: return "Audio"
                case .Movie: return "Movie"
                case .Other: return "Other"
            }
        }
    }
    
    struct Item: CustomStringConvertible
    {
        let URL: NSURL!
        let type: ItemType!
        
        var description: String {
            return "URL = \(URL); Type = \(type)"
        }
    }
    
    private let fileManager = NSFileManager.defaultManager()
    private let rootDirectoryURL: NSURL!
    
    let title: String!
    
    private var items: [Item]?
    
    subscript(index: Int) -> Item? {
        if let items = items where items.count > index {
            return items[index]
        }
        return nil
    }
    
    var count: Int {
        return items?.count ?? 0
    }
    
    init(rootDirectoryURL: NSURL) {
        self.rootDirectoryURL = rootDirectoryURL
        title = self.rootDirectoryURL.lastPathComponent ?? "Untitled"
        let content = try? fileManager.contentsOfDirectoryAtURL(self.rootDirectoryURL, includingPropertiesForKeys: nil, options: NSDirectoryEnumerationOptions.SkipsSubdirectoryDescendants)
        
        items = content?.map({ URL in
            return Item(URL: URL, type: self.itemTypeFromURL(URL))
        })
    }
    
    func deleteItemAtIndex(index: Int) -> Bool {
        if let item = self[index] {
            if fileManager.fileExistsAtPath(item.URL.path!) {
                var error: NSError?
                do {
                    try fileManager.removeItemAtURL(item.URL)
                } catch let error1 as NSError {
                    error = error1
                }
                items!.removeAtIndex(index)
                return error == nil
            }
        }
        return false
    }
    
    private func itemTypeFromURL(URL: NSURL) -> ItemType {
        if isURLDirectory(URL) {
            return ItemType.Directory
        } else if let pathExtension = URL.pathExtension {
            let fileUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as CFStringRef, nil)!.takeRetainedValue()
            if UTTypeConformsTo(fileUTI, kUTTypeImage) {
                return ItemType.Image
            } else if UTTypeConformsTo(fileUTI, kUTTypeAudio) {
                return ItemType.Audio
            } else if UTTypeConformsTo(fileUTI, kUTTypeMovie) {
                return ItemType.Movie
            }
            return ItemType.Other
        }
        return ItemType.Other
    }
    
    private func isURLDirectory(URL: NSURL) -> Bool {
        var isDirectory: AnyObject?
        do {
            try URL.getResourceValue(&isDirectory, forKey: NSURLIsDirectoryKey)
        } catch _ {
        }
        return (isDirectory as? NSNumber)?.boolValue ?? false
    }
    
}
