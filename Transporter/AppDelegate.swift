//
//  AppDelegate.swift
//  Transporter
//
//  Created by Sauron Black on 4/4/15.
//  Copyright (c) 2015 Totenkopf. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        return true
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        print("\(url)")
        return true
    }
}

