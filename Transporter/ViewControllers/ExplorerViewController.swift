//
//  ViewController.swift
//  Transporter
//
//  Created by Sauron Black on 4/4/15.
//  Copyright (c) 2015 Totenkopf. All rights reserved.
//

import UIKit
import MediaPlayer
import AVFoundation

class ExplorerViewController: UIViewController
{
    private struct Constants
    {
        static let ItemTableViewCellReuseIdentifier = "ItemTableViewCell"
        static let StoryboardId = "ExplorerViewController"
        static let FolderIconeName = "folder_icone"
        static let AudioFileIcone = "audio_file_icone"
    }

    @IBOutlet weak var tableView: UITableView!
    
	private lazy var documentsDirectoryURL: NSURL = {
       NSFileManager.defaultManager().URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
    }()
    private var explorer: Explorer?
    private let thumbnailCache = NSCache()
    
    var rootDirectoryURL: NSURL? {
        didSet {
            if let URL = rootDirectoryURL {
                explorer = Explorer(rootDirectoryURL: URL)
            }
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if explorer == nil {
            explorer = Explorer(rootDirectoryURL: documentsDirectoryURL)
        }
        self.title = explorer!.title
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRowAtIndexPath(selectedIndexPath, animated: true)
        }
    }
    
    deinit {
        print("\(self) ist Tod")
    }
    
    // MARK: - Private
    
    private func openDirectoryAtURL(URL: NSURL) {
        let explorerViewController = self.storyboard!.instantiateViewControllerWithIdentifier(Constants.StoryboardId) as! ExplorerViewController
        explorerViewController.rootDirectoryURL = URL
        self.navigationController!.pushViewController(explorerViewController, animated: true)
    }
    
    private func openMediaAtURL(URL: NSURL) {
        let player = MPMoviePlayerViewController(contentURL: URL)
        presentViewController(player, animated: true, completion: nil)
        player.moviePlayer.play()
    }
    
    private func thumbnaillForExplorerItem(item: Explorer.Item) -> UIImage? {
        switch item.type! {
            case Explorer.ItemType.Movie:
                return thumbnaillForMovieAtURL(item.URL)
            case Explorer.ItemType.Directory:
                return UIImage(named: Constants.FolderIconeName)
            case Explorer.ItemType.Audio:
                return UIImage(named: Constants.AudioFileIcone)
            default:
                break;
        }
        return nil
    }
    
    private func thumbnaillForMovieAtURL(url: NSURL) -> UIImage? {
        if let cachedImage = thumbnailCache.objectForKey(url) as? UIImage {
            return cachedImage
        }
        let asset = AVURLAsset(URL: url, options: nil)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.maximumSize = CGSize(width: 300, height: 300)
        let time = CMTimeMakeWithSeconds(2, 30);
		let cgImage = try? imageGenerator.copyCGImageAtTime(time, actualTime: nil)
        if let cgImage = cgImage {
			let image = UIImage(CGImage: cgImage)
            thumbnailCache.setObject(image, forKey: url)
            return image
        }
        return nil
    }
    
    // MARK: - Actions
    
    @IBAction func toggleEditMode(sender: UIBarButtonItem) {
        tableView.setEditing(!tableView.editing, animated: true)
        sender.title = tableView.editing ? "Done": "Edit"
    }
}

// MARK: - UITableViewDataSource

extension ExplorerViewController: UITableViewDataSource
{
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return explorer?.count ?? 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(Constants.ItemTableViewCellReuseIdentifier) as! ItemTableViewCell
        let item = explorer![indexPath.row]!
        cell.itemTitleLabel.text = item.URL.lastPathComponent
        cell.itemImageView.image = thumbnaillForExplorerItem(item)
        return cell
    }
}

// MARK: - UITableViewDelegate

extension ExplorerViewController: UITableViewDelegate
{
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let item = explorer![indexPath.row]!
        switch item.type! {
            case Explorer.ItemType.Directory:
                openDirectoryAtURL(item.URL)
            case Explorer.ItemType.Movie, Explorer.ItemType.Audio:
                openMediaAtURL(item.URL)
            default:
                break;
        }
        print("\(item)")
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        return [UITableViewRowAction(style: UITableViewRowActionStyle.Default, title: "Delete", handler: { (_, indexPath) -> Void in
            if self.explorer!.deleteItemAtIndex(indexPath.row) {
                self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
            }
        })]
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        //
    }
    
    func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        return UITableViewCellEditingStyle.Delete
    }
}
