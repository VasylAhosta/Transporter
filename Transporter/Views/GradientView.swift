//
//  GradientView.swift
//  Transporter
//
//  Created by Sauron Black on 4/8/15.
//  Copyright (c) 2015 Totenkopf. All rights reserved.
//

import UIKit
import CoreGraphics

class GradientView: UIView
{
    private struct Constants
    {
        static let StartColor = UIColor(hexColor: 0x465D43)
        static let EndColor = UIColor(hexColor: 0x1D261B)
    }
    
    override func drawRect(rect: CGRect) {
        var context = UIGraphicsGetCurrentContext()
        CGContextSaveGState(context)
        
        var colorSpace = CGColorSpaceCreateDeviceRGB()
        var colors = [Constants.StartColor.CGColor, Constants.EndColor.CGColor]
        var locations: [CGFloat] = [0.0, 1.0]
        var gradient = CGGradientCreateWithColors(colorSpace, colors, &locations)
        
        var startPoint = CGPointMake(0.0, 0.0)
        var endPoint = CGPointMake(0.0, CGRectGetHeight(self.bounds))
        CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, CGGradientDrawingOptions())
        
        CGContextRestoreGState(context)
    }
}

extension UIColor
{
    convenience init(hexColor: Int, alpha: CGFloat) {
        let red: CGFloat = CGFloat((hexColor >> 16) & 0xff) / 255.0
        let green: CGFloat = CGFloat((hexColor >> 8) & 0xff) / 255.0
        let blue: CGFloat = CGFloat(hexColor & 0xff) / 255.0
        self.init(red: red, green: green, blue: blue, alpha: 1.0)
    }
    
    convenience init(hexColor: Int) {
        self.init(hexColor: hexColor, alpha: 1.0)
    }
}
